# API for managing characters and movies with ASP.NET Core and C#
Store and manage your data - The API handels requests regarding movies, characters, franchises and actors. 

## Getting started and usage
1. Clone to a local directory.
2. Open solution in Visual Studio (VS).
3. Insert your credentials in the ```appsettings.json``` file under the *ConnectionStrings* propertie and then *Default*.
Under the *ConfigureServices* in ```Startup.cs``` the database context is added as an SQL server.
    - ```Data Source```-> database
    - ```Initial Catalog``` -> name on database (will make database for you if it does not exist).
4. To run the migrations:
    - .NET Core CLI -> ```dotnet ef database update```
    - NuGet PowerShell -> ```update-database```

Running the program from VS (hotkey: F5) and a browser will open a new page in your browser with generated documentation using [Swagger]. From here you can run all CRUD opertaions and test the API.

For more information about migrations see documentation [Entity Framework Core].
## Check it out in Postman
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/5a72a31224ee5f9e6a20)

## Prerequisites
* .NET Framework
* Visual Studio 2017/19 OR Visual Studio Code

### Packages:
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.Sqlite
* Microsoft.EntityFrameworkCore.Tools
* AutoMapper.Extensions.Microsoft.DependencyInjection
* Packages for Swagger
* (Microsoft.VisualStudio.Web.CodeGeneration.Design - not needed to run api, but is used for generating basic Controllers for Models)


[//]: #
[Entity Framework Core]: <https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli>
[Swagger]: <https://swagger.io/>
