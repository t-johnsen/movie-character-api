﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_character_api.DTO.Actor;
using movie_character_api.DTO.Character;
using movie_character_api.DTO.Movie;
using movie_character_api.Model;

namespace movie_character_api.Controllers
{
    /// <summary>
    /// Controller for Actor entity. Normal CRUD operations as well as some custom GET requests
    /// to show the relationships between the entities.
    /// </summary>
    [Route("api/v1/actor")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/actor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActors()
        {
            List<Actor> actors =  await _context.Actors.ToListAsync();

            return actors.Select(a => _mapper.Map<ActorDto>(a)).ToList();
        }

        // GET: api/actor/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return _mapper.Map<Actor, ActorDto>(actor);
        }

        // GET: api/actor/5/movie
        [HttpGet("{id}/movie")]
        public async Task<ActionResult<ActorMovieDto>> GetActorMovies(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null) return NotFound();

            var moviesForActor = await _context.MovieCharacters
                .Include(mc => mc.Movie)
                .Where(mc => mc.ActorId == id)
                .ToListAsync();

            var movies = moviesForActor.Select(mc => mc.Movie).Distinct().ToList();

            ActorMovieDto actorMovieDto = new ActorMovieDto()
            {
                ActorId = actor.Id,
                ActorFullName = actor.Firstname + " " + actor.Lastname,
                MovieDtos = movies.Select( m => _mapper.Map<Movie, MovieDto>(m)).ToList()
            };

            return actorMovieDto;
        }

        // GET: api/actor/5/character
        [HttpGet("{id}/character")]
        public async Task<ActionResult<ActorCharacterDto>> GetActorCharacters(int id)
        {
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null) return NotFound();

            var movieCharacters = await _context.MovieCharacters
                .Include(mc => mc.Character)
                .Where(mc => mc.ActorId == id)
                .ToListAsync();

            var characters = movieCharacters.Select(mc => mc.Character).Distinct().ToList();

            ActorCharacterDto actorMovieDto = new ActorCharacterDto()
            {
                Id = actor.Id,
                FullName = actor.Firstname + " " + actor.Lastname,
                CharacterDtos = characters.Select(c => _mapper.Map<Character, CharacterDto>(c)).ToList()
            };

            return actorMovieDto;
        }

        // PUT: api/actor/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/actor
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ActorDto>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            ActorDto actorDto = _mapper.Map<Actor, ActorDto>(actor);

            return CreatedAtAction("GetActor", new { id = actorDto.Id }, actorDto);
        }

        // DELETE: api/actor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActorDto>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return _mapper.Map<Actor, ActorDto>(actor);
        }

        /// <summary>
        /// Helper method to check if actor exists.
        /// </summary>
        /// <param name="id">The id of the actor object</param>
        /// <returns>True/false</returns>
        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
