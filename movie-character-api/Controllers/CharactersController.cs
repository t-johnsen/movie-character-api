﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_character_api.DTO.Actor;
using movie_character_api.DTO.Character;
using movie_character_api.Model;

namespace movie_character_api.Controllers
{
    /// <summary>
    /// Controller for Character entity. Normal CRUD operations as well as some custom GET requests
    /// to show the relationships between the entities.
    /// </summary>
    [Route("api/v1/character")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacters()
        {
            return await _context.Characters.Select(c => _mapper.Map<Character, CharacterDto>(c)).ToListAsync();
        }

        // GET: api/v1/character/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<Character, CharacterDto>(character);
        }

        // GET: api/v1/character/5/actor
        [HttpGet("{id}/actor")]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetCharactersActors(int id)
        {
            var mc = await _context.MovieCharacters
                .Include(mc => mc.Actor)
                .Where(mc => mc.CharacterId == id)
                .ToListAsync();

            List<Actor> distinctActors = mc.Select(mc => mc.Actor).Distinct().ToList();

            List<ActorDto> actorDtos = distinctActors.Select(a => _mapper.Map<Actor, ActorDto>(a)).ToList();

            return actorDtos;
        }

        // PUT: api/v1/character/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/character
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CharacterDto>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, _mapper.Map<Character, CharacterDto>(character));
        }

        // DELETE: api/v1/character/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterDto>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return _mapper.Map<Character, CharacterDto>(character);
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
