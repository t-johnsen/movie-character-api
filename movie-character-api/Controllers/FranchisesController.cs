﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_character_api.DTO.Character;
using movie_character_api.DTO.Franchise;
using movie_character_api.DTO.Movie;
using movie_character_api.Model;

namespace movie_character_api.Controllers
{
    /// <summary>
    /// Controller for Franchise entity. Normal CRUD operations as well as some custom GET requests
    /// to show the relationships between the entities.
    /// </summary>
    [Route("api/v1/franchise")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            return await _context.Franchises.Select(f => _mapper.Map<FranchiseDto>(f)).ToListAsync();
        }

        // GET: api/v1/franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<Franchise, FranchiseDto>(franchise);
        }

        // GET: api/v1/franchise/5/character
        [HttpGet("{id}/character")]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetFranchiseCharacters(int id)
        {
            var characters = await _context.MovieCharacters.
                 Where(mc => mc.Movie.FranchiseId == id)
                 .Select(mc => mc.Character)
                 .Distinct()
                 .ToListAsync();
            List<CharacterDto> charDto = _mapper.Map<List<CharacterDto>>(characters);

            return charDto;
        }

        // Gets all movies in a specified franchise
        // GET: api/v1/franchise/5/movie
        [HttpGet("{id}/movie")]
        public async Task<ActionResult<FranchiseMovieDto>> GetFranchiseMovie(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            
            if (franchise == null) return NotFound();

            var movies = await _context.Movies
                .Include(m => m.Franchise)
                .Where(m => m.FranchiseId == id)
                .ToListAsync();

            List<MovieDto> mfdto = movies.Select(m => _mapper.Map<Movie, MovieDto>(m)).ToList();

            return new FranchiseMovieDto()
            {
                Id = franchise.Id,
                Name = franchise.Name,
                MovieDtos = mfdto
            };
        }


        // PUT: api/v1/franchise/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/franchise
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FranchiseDto>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<Franchise, FranchiseDto>(franchise));
        }

        // DELETE: api/v1/franchise/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDto>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return _mapper.Map<Franchise, FranchiseDto>(franchise);
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
