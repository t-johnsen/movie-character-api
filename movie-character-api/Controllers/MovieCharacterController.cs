﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_character_api.DTO.Movie;
using movie_character_api.Model;


namespace movie_character_api.Controllers
{
    /// <summary>
    /// Controller for MovieCharacter entity. This is the joining relationship between
    /// Movie entity and Character entity with simple CRUD operations to allow for modification.
    /// </summary>
    [Route("api/v1/movie-character")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        // GET: api/v1/movie-character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacter()
        {
            return await _context.MovieCharacters.Select(mc => _mapper.Map<MovieCharacter, MovieCharacterDto>(mc)).ToListAsync();
        }

        // POST: api/v1/movie-character
        [HttpPost]
        public async Task<ActionResult<MovieCharacterDto>> PostMovieCharacter(MovieCharacter movieCharactersInput)
        {
            try
            {
                _context.Add(movieCharactersInput);
                await _context.SaveChangesAsync();
            }
            catch (DbException)
            {
                if (MovieCharacterExists(movieCharactersInput.CharacterId, movieCharactersInput.MovieId))
                {
                    return BadRequest();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new
            {
                movieCharactersInput.CharacterId,
                movieCharactersInput.MovieId,
                movieCharactersInput.ActorId
            }, _mapper.Map<MovieCharacter, MovieCharacterDto>(movieCharactersInput)
            );
        }

        // PUT: api/v1/movie-character
        [HttpPut]
        public async Task<IActionResult> PutMovieCharacter(MovieCharacter movieCharacter)
        {

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                if (!MovieCharacterExists(movieCharacter.CharacterId, movieCharacter.MovieId))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest();
                }
            }

            return NoContent();
        }

        // DELETE: api/v1/movie-character
        [HttpDelete]
        public async Task<ActionResult<MovieCharacterOnlyDto>> DeleteMovieCharacter(MovieCharacter inputMovieCharacter)
        {
            if (!MovieCharacterExists(inputMovieCharacter.CharacterId, inputMovieCharacter.MovieId))
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(inputMovieCharacter);
            await _context.SaveChangesAsync();

            return _mapper.Map<MovieCharacter, MovieCharacterOnlyDto>(inputMovieCharacter);
        }


        private bool MovieCharacterExists(int characterId, int movieId)
        {
            return _context.MovieCharacters.Any(e => e.CharacterId == characterId && e.MovieId == movieId);
        }

    }
}
