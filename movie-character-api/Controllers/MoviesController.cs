﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie_character_api.DTO.Character;
using movie_character_api.DTO.Movie;
using movie_character_api.Model;

namespace movie_character_api.Controllers
{
    /// <summary>
    /// Controller for Movie entity. Normal CRUD operations as well as some custom GET requests
    /// to show the relationships between the entities.
    /// </summary>
    [Route("api/v1/movie")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/movie
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
        {
            return await _context.Movies.Select(m => _mapper.Map<Movie, MovieDto>(m)).ToListAsync();
        }

        // GET: api/v1/movie/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<Movie, MovieDto>(movie);
        }

        // GET: api/v1/v1/movie/5/character/actor
        [HttpGet("{id}/character/actor")]
        public async Task<ActionResult<MovieCharacterActorDto>> GetMovieCharactersAndActors(int id)
        {

            var movie = await _context.Movies.FindAsync(id);

            if (movie == null) return NotFound();

            var charAct = await _context.MovieCharacters
                .Include(mc => mc.Character)
                .Include(mc => mc.Actor)
                .Where(mc => mc.MovieId == id)
                .ToListAsync();

            MovieCharacterActorDto movieCharacterActorDto = new MovieCharacterActorDto() 
            { 
                Id = movie.Id,
                MovieTitle = movie.MovieTitle,
                CharacterActorDtos = charAct.Select(
                ca => new CharacterActorDto()
                {
                    ActorId = ca.Actor.Id,
                    ActorName = ca.Actor.Firstname + " " + ca.Actor.Lastname,
                    CharacterID = ca.Character.Id,
                    CharacterName = ca.Character.FullName
                })
                .ToList()
            };


            return movieCharacterActorDto;
        }

        // PUT: api/v1/movie/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {

            if (id != movie.Id)
            {
                return BadRequest();
            }


            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/movie
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieDto>> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, _mapper.Map<Movie, MovieDto>(movie));
        }

        // DELETE: api/v1/movie/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDto>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return _mapper.Map<Movie, MovieDto>(movie);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
