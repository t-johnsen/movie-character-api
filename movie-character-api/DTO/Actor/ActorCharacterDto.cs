﻿using movie_character_api.DTO.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Actor
{
    public class ActorCharacterDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public List<CharacterDto> CharacterDtos { get; set; }
    }
}
