﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Actor
{
    public class ActorDto
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string OtherName { get; set; }
        public string Lastname { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Biography { get; set; }
        public Uri Picture { get; set; }
    }
}
