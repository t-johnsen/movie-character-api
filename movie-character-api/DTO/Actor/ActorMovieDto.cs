﻿using movie_character_api.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Actor
{
    public class ActorMovieDto
    {
        public int ActorId { get; set; }
        public string ActorFullName { get; set; }
        public List<MovieDto> MovieDtos { get; set; }

    }
}
