﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Character
{
    public class CharacterActorDto
    {
        public int CharacterID { get; set; }
        public string CharacterName { get; set; }
        public int ActorId { get; set; }
        public string ActorName { get; set; }


    }
}
