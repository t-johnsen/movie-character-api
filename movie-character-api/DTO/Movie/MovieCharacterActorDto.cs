﻿using movie_character_api.DTO.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Movie
{
    public class MovieCharacterActorDto
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public List<CharacterActorDto> CharacterActorDtos { get; set; }

    }
}
