﻿namespace movie_character_api.Controllers
{
    public class MovieCharacterDto
    {
        public int CharacterId { get; set; }
        public int MovieId { get; set; }
        public int ActorId { get; set; }

    }
}