﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Movie
{
    public class MovieCharacterOnlyDto
    {
        public int CharacterId { get; set; }
        public int MovieId { get; set; }
    }
}
