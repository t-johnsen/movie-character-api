﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.DTO.Movie
{
    public class MovieDto
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public Uri Picture { get; set; }
        public Uri Trailer { get; set; }
        public int FranchiseId { get; set; }
    }
}
