﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace movie_character_api.Migrations
{
    public partial class UpdatedDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Name", "Description" },
                values: new object[,]
                {
                    {1, "Franchise1", "This is not a legit desciption for this franchise" },
                    {2, "Franchise2", "This is not a legit desciption for this franchise" },
                    {3, "Franchise3", "This is not a legit desciption for this franchise" },
                    {4, "Franchise4", "This is not a legit desciption for this franchise" },
                    {5, "Franchise5", "This is not a legit desciption for this franchise" },
                    {6, "Franchise6", "This is not a legit desciption for this franchise" },
                    {7, "Franchise7", "This is not a legit desciption for this franchise" }
                }
                );

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "MovieTitle", "Genre", "ReleaseYear", "Director", "Picture", "Trailer", "FranchiseId" },
                values: new object[,]
                {
                    {1, "Movie1", "Action", 1992, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 1 },
                    {2, "Movie2", "Action", 1993, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 1 },
                    {3, "Movie3", "Action", 1994, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 2 },
                    {4, "Movie4", "Action", 1995, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 3 },
                    {5, "Movie5", "Action", 1996, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 4 },
                    {6, "Movie6", "Action", 1996, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 5 },
                    {7, "Movie7", "Action", 1994, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 6 },
                    {8, "Movie8", "Action", 1996, "This is a director", "https://yt3.ggpht.com/a/AATXAJw3vuFrU1lEZ4YLDHp7XowER52Vb22pC-oz_GvilQ=s900-c-k-c0xffffffff-no-rj-mo", "https://youtu.be/xa_z57UatDY", 6 }

                }
                );

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Alias", "Gender", "Picture" },
                values: new object[,]
                {
                    {1, "Character1", "Alias1", "Male", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" },
                    {2, "Character2", "Alias2", "Female", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" },
                    {3, "Character3", "Alias3", "Female", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" },
                    {4, "Character4", "Alias4", "Female", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" },
                    {5, "Character5", "Alias5", "Male", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" },
                    {6, "Character6", "Alias6", "Female", "https://upload.wikimedia.org/wikipedia/en/thumb/3/3b/SpongeBob_SquarePants_character.svg/1200px-SpongeBob_SquarePants_character.svg.png" }
                }
                );

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Firstname", "OtherName", "Lastname", "Gender", "DOB", "PlaceOfBirth", "Biography", "Picture" },
                values: new object[,]
                {
                    {1, "Firstname1", "NoOtherName", "Lastname1", "Male", new DateTime(2000, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"},
                    {2, "Firstname2", "NoOtherName", "Lastname2", "Male", new DateTime(1993, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"},
                    {3, "Firstname3", "NoOtherName", "Lastname3", "Female", new DateTime(1990, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"},
                    {4, "Firstname4", "NoOtherName", "Lastname4", "Female", new DateTime(1974, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"},
                    {5, "Firstname5", "NoOtherName", "Lastname5", "Female", new DateTime(1960, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"},
                    {6, "Firstname6", "NoOtherName", "Lastname6", "Male", new DateTime(1960, 02, 01), "Norway", "This is my biography", "https://images.newindianexpress.com/uploads/user/imagelibrary/2020/2/10/w1200X800/vijay_sarakr.jpg"}
                }
                );

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharacterId", "MovieId", "ActorID" },
                values: new object[,]
                {
                    {1, 2, 1 },
                    {2, 3, 2 },
                    {3, 1, 3 },
                    {4, 5, 4 },
                    {5, 6, 5 },
                    {6, 4, 6 },
                    {3, 7, 5 },
                    {1, 4, 1 },
                    {1, 8, 2 },
                    {6, 1, 2 },
                    {5, 2, 5 },
                    {2, 8, 5 }

                }
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValues: new object[] {1, 2, 3, 4, 5, 6, 7, 8 }
                );

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValues: new object[] {1, 2, 3, 4, 5, 6 }
                );

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValues: new object[] {1, 2, 3, 4, 5, 6, 7 }
                );

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new string[] {"CharacterId", "MovieId" },
                keyValues: new object[,] {
                    {1, 2},
                    {2, 3},
                    {3, 1},
                    {4, 5},
                    {5, 6},
                    {6, 4},
                    {3, 7},
                    {1, 4},
                    {1, 8},
                    {6, 1},
                    {5, 2},
                    {2, 8}
                }
                );

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValues: new object[] {1, 2, 3, 4, 5, 6 }
                );
        }
    }
}
