﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace movie_character_api.Model
{
    public class Character
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public Uri Picture { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }

    }
}
