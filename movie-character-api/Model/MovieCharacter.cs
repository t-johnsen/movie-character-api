﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.Model
{
    public class MovieCharacter : IEquatable<MovieCharacter>
    {
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        public int ActorId { get; set; }
        public Actor Actor { get; set; }

        public bool Equals([AllowNull] MovieCharacter other)
        {
            if(this.CharacterId == other.CharacterId && 
                this.MovieId == other.MovieId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
