﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.Model
{
    /// <summary>
    /// Class which represents a session with the database where you configure
    /// the database sets represented in your database.
    /// It's used to query and save instances of your entities.
    /// DbContext is a combination of the Unit Of Work and Repository pattern.
    /// </summary>
    public class MovieCharacterDbContext : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MovieCharacterDbContext(DbContextOptions options) : base(options) { }


        // Configuring of composite key between Movie and Character
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });
        }

    }
}
