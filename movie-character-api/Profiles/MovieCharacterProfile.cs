﻿using AutoMapper;
using movie_character_api.Controllers;
using movie_character_api.DTO.Movie;
using movie_character_api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie_character_api.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterDto>();
            CreateMap<MovieCharacter, MovieCharacterOnlyDto>();
        }
    }
}
